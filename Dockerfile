FROM linuxserver/code-server

RUN sudo apt-get update && \
    sudo apt-get install -y locales locales-all

# Set the locale
RUN sudo sed -i '/de_DE.UTF-8/s/^# //g' /etc/locale.gen && \
    sudo locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

RUN sudo apt-get install -y software-properties-common && \
    sudo add-apt-repository -y ppa:ondrej/php && \
    sudo apt-get update && \
    sudo apt upgrade -y && \
    sudo apt-get install -y php8.1 -y && \
    sudo rm -rf /var/lib/apt/lists/*  && \
    sudo apt-get clean